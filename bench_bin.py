import cProfile
import pstats
from pstats import SortKey
from src.app import util
import bench_util
import matplotlib.pyplot as plt
import numpy as np

operation_lst = []
len_array = []

for i in range(2, 319, 1):
    lst = bench_util.create_lst(i)
    sorted(lst)
    index = util.indexs
    temp = 0
    for j in range(2):
        cProfile.run('util.binary_search(lst, index)', 'stats.log')
        with open('../output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
        f = open('../output.txt')
        line = bench_util.correct_lines(f)
        f.close()
        temp += int(line)
    operation_lst.append(temp//5)
    len_array.append(i)
print(len_array)
print(operation_lst)

teory_operation = list(operation_lst)
np.seterr(divide = 'ignore')
teory_hard = [ np.log(i) for i in range(len(teory_operation))]
print(teory_hard)

plt.title('"Бинарный поиск"')


# plt.plot(len_array, operation_lst, c='green', label='Сортировка')
# plt.plot(len_array, teory_hard, c='red', label='Теор. сложность')
plt.grid()
plt.legend()
plt.xlabel('Сложность алгоритма')
plt.ylabel('Количество операций ')


plt.show()
